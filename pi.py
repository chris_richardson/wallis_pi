# Calculate approximate value of π using Wallis formula
def wallis(nmax):
    a = 1.0
    for n in range(2, nmax, 2):
        a *= n*n/((n + 1)*(n - 1))
    return 2*a

# Print result after a million iterations
print(wallis(1000000))

# Unit test to make sure it is roughly working - use a million iterations
import pytest
def test_wallis():
    n = 1000000
    assert abs(wallis(n) - 3.14159265358979) < 1e-5


